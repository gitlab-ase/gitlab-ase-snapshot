# frozen_string_literal: true

module GiphyDecorator
  extend ActiveSupport::Concern

  included do
    before_action :push_giphy_gon, if: :html_request?

    content_security_policy do |p|
      next if p.directives.blank?

      default_connect_src = p.directives['connect-src'] || p.directives['default-src']
      connect_src_values = Array.wrap(default_connect_src) | [::Gitlab::Giphy.api_url]
      p.connect_src(*connect_src_values)
    end
  end

  private

  def push_giphy_gon
    gon.push({
      giphy: {
        api_token: ::Gitlab::Giphy.api_token,
        api_url: ::Gitlab::Giphy.api_url
      }
    })
  end
end
