# Vue Project

## Overview

This project is for beginners to Vue. As you work on solving the requirements,
use the [vue guide](https://vuejs.org/v2/guide/), peers, and other resources to 
learn more about Vue.

**PLEASE NOTE:** We will be using [Vue 2](https://vuejs.org/v2/guide/), **not** [Vue 3](https://v3.vuejs.org/guide/introduction.html).

## Initial Setup

1. Create a CodeSandbox account by visiting https://codesandbox.io/ and clicking
   **Sign in** at the top. You will need to have either a GitHub or Google account
   to authenticate with codesandbox.
2. Make sure you are signed into CodeSandbox whenever you are working on this project.
3. Visit [the `chess960-shuffler-start` project](https://codesandbox.io/s/chess960-shuffler-start-wgbyy).
   This project contains the starting code, which you will need to modify to satisfy the
   requirements.
4. Click **Fork** in the top right to create a copy of this project under your user's
   account. You might want to bookmark the URL of your fork. This is where you'll
   be working and saving your changes.

After completing these steps, you should see three panes:

1. A file tree
2. A file editor
3. A preview of the app. (You might need to wait a minute or two for the sandbox to build).

![screenshot](./vue_1.png)

- Feel free to make any changes to any file.
- The preview will automatically refresh as you make a change. If you're in the middle of a change, don't
  be surprised if the preview breaks and shows compilation errors. After you finish your change, the files
  should be compile-able again and the preview will automatically update.
- You will still need to save changes to your files with CTRL+S (or CMD+S on Mac).

## Requirements

### Requirement 1 - Shuffle

When a user clicks the "Shuffle" button, then the chess pieces should be placed in a random order on the row.

| Before "Shuffle" pressed | After "Shuffle" clicked |
|-----|------|
| ![before](./vue_2.png) | ![req 1 screenshot](./vue_3.png) |

Details:

- Before the "Shuffle" button is ever hit, the pieces should start in their classical order: rook, knight, bishop, queen, king, bishop, knight, rook (from left to right on white's side of the board).

HINT:

- You can either implement your own shuffle function, or add a dependency to [`lodash`](https://lodash.com)
  and use their [`shuffle`](https://lodash.com/docs/4.17.15#shuffle) function.  
- When using `v-for` be sure to also provide a [`:key`](https://vuejs.org/v2/api/#key) binding.
  In our situation, you can just use the loop's `index` as the `:key` (see [relevant docs](https://vuejs.org/v2/guide/list.html#Mapping-an-Array-to-Elements-with-v-for)).

### Requirement 2 - "Black" and "White" Squares

When the user sees the chess pieces, then the squares should alternate between "black" and "white", such that the square on the far right is "white".

| Before "Shuffle" pressed | After "Shuffle" clicked |
|-----|------|
| ![before](./vue_5.png) | ![req 1 screenshot](./vue_4.png) |

Details:

- "White" squares should have a background color of `#ddd`, and "Black" squares should have a background color of `#aaa`.

HINT:

- You will likely need to add a property to the `ChessSquare` component (see [relevant Vue docs](https://vuejs.org/v2/guide/components-props.html)).

### Requirement 3 - Fischer Random Rules

When a user clicks the "Shuffle" button, then the chess pieces should be placed in a random order on the row, according to the constraints of [Fischer random chess](https://en.wikipedia.org/wiki/Fischer_random_chess#Setup).

- The bishops must be placed on opposite color squares.
- The king must be placed on a square between the two rooks.

| Example 1 | Example 2 | Example 3 |
|------------|----|-----|
| ![example 1](./vue_6.png) | ![example 2](./vue_7.png) | ![example 3](./vue_8.png) |

HINT:

- You'll likely want to replace the "shuffle" method you added in Requirement 1 with a method that generates new arrays of length 8 whose contents are pieces randomly placed in a valid Fischer random configurations.
- Here's a good algorithm for generating valid Fischer random starting position is:
  1. Create an empty array of length 8
  2. Randomly place the 2 bishops on the white/black squares.
  3. Randomly place the 2 knights and queen on any empty square.
  4. The 2 rooks and king, must fill in the remaining 3 squares with the king between the two rooks.
- You might want to create a function `getRandomEmptyIndex` which takes in an array, and returns a random empty index.
- If you finished the previous requirements correctly, you shouldn't need to change anything in a `<template>`. This highlights the benefits of Vue's ability to separate logic from presentation.
