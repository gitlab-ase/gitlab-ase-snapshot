# GitLab - Advanced Software Engineering Snapshot

This is a snapshot of the [GitLab project](https://gitlab.com/gitlab-org/gitlab), used for projects and assignments of GitLab's Advanced Software Engineering course.

## Links

- [Vue Project Instructions](./ase_projects/PROJECT_VUE.md)
- [Giphy Project Instructions](./ase_projects/PROJECT_GIPHY.md)
