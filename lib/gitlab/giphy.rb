
# frozen_string_literal: true

module Gitlab
  class Giphy
    def self.api_url
      Gitlab.config.giphy&.api_url || 'http://api.giphy.com/v1/'
    end

    def self.api_token
      token = Gitlab.config.giphy.api_token

      raise "Expected to find giphy.api_token in config/gitlab.yml." unless token

      token
    end
  end
end
